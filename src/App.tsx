import React, { useEffect, useState } from 'react'
import logo from './logo.svg'
import './App.css'
import { SSL_OP_LEGACY_SERVER_CONNECT } from 'constants'

const { random, floor, abs, min } = Math

function safeDegree(degree: number) {
  return (degree + 360) % 360
}

function isInRange(target: number, left: number, right: number) {
  if (right < left) {
    if (left <= target && target <= right + 360) {
      return true
    }
    if (left - 360 <= target && target <= right) {
      return true
    }
  }
  if (left <= target && target <= right) {
    return true
  }
  return false
}

// function isInRangeTest(target: number, left: number, right: number) {
//   console.log({
//     in: isInRange(target, left, right),
//     target,
//     left,
//     right,
//   })
// }

// console.log('== true ==')
// isInRangeTest(180, 179, 181)
// isInRangeTest(359, 350, 50)
// isInRangeTest(1, 350, 50)

// console.log('== false ==')
// isInRangeTest(180, 170, 175)
// isInRangeTest(180, 185, 190)
// isInRangeTest(180, 350, 50)

function isLeftRight(left: number, right: number) {
  const distance = safeDegree(right - left)
  return distance < 180
}

// function isLeftRightTest(left: number, right: number) {
//   console.log({
//     is: isLeftRight(left, right),
//     left,
//     right,
//   })
// }

// console.log('== true ==')
// isLeftRightTest(100, 110)
// isLeftRightTest(349, 359)
// isLeftRightTest(350, 0)
// isLeftRightTest(351, 1)

// console.log('== false ==')
// isLeftRightTest(110, 100)
// isLeftRightTest(359, 349)
// isLeftRightTest(0, 350)
// isLeftRightTest(1, 351)

function App() {
  const [targetDegree, setTargetDegree] = useState(89)
  const [viewDegree, setViewDegree] = useState(0)

  const distanceToCenter = min(
    abs(viewDegree - targetDegree),
    abs(viewDegree + 360 - targetDegree),
    abs(viewDegree - (targetDegree + 360)),
  )

  const viewWidth = 135
  const viewLeftDegree = safeDegree(viewDegree - viewWidth / 2)
  const viewRightDegree = safeDegree(viewDegree + viewWidth / 2)
  const isInView = isInRange(targetDegree, viewLeftDegree, viewRightDegree)

  const handWidth = 30
  const handLeftDegree = safeDegree(viewDegree - handWidth / 2)
  const handRightDegree = safeDegree(viewDegree + handWidth / 2)
  const isInHand = isInRange(targetDegree, handLeftDegree, handRightDegree)

  const worldWidth = 450
  const worldHeight = 320

  const ballSize = 32
  const ballTop = worldHeight / 2 - ballSize / 2

  const ballLeft =
    worldWidth -
    (isLeftRight(viewDegree, targetDegree)
      ? worldWidth / 2 - (distanceToCenter / (viewWidth / 2)) * (worldWidth / 2)
      : (distanceToCenter / (viewWidth / 2)) * (worldWidth / 2) +
        worldWidth / 2)

  const handWidthPx = (handWidth / viewWidth) * worldWidth

  function changeView(delta: number) {
    setViewDegree(x => safeDegree(x + delta))
  }

  useEffect(() => {
    function onKey(event: KeyboardEvent) {
      if (event.key === '4') {
        changeView(-1)
      } else if (event.key === '6') {
        changeView(1)
      }
    }
    window.addEventListener('keypress', onKey)
    return () => window.removeEventListener('keypress', onKey)
  }, [])

  return (
    <div className="App">
      <div>target: {targetDegree}</div>
      <div>view: {viewDegree}</div>
      <div>distance: {distanceToCenter}</div>
      <div className="padding">
        <div>view left: {viewLeftDegree}</div>
        <div>view right: {viewRightDegree}</div>
        <div>is in view: {isInView ? 'yes' : 'no'}</div>
      </div>
      <div className="padding">
        <div>hand left: {handLeftDegree}</div>
        <div>hand right: {handRightDegree}</div>
        <div>is in hand: {isInHand ? 'yes' : 'no'}</div>
      </div>
      <div className="padding"></div>

      <div>
        <button onClick={() => changeView(-1)}>&larr;</button>
        <button onClick={() => changeView(1)}>&rarr;</button>
      </div>
      <div
        className="world"
        style={{ width: worldWidth + 'px', height: worldHeight + 'px' }}
      >
        <div
          className="hand"
          style={{
            top: 0 + 'px',
            bottom: worldHeight + 'px',
            width: handWidthPx + 'px',
            left: worldWidth / 2 + 'px',
            transform: `translate(-50%,0)`,
          }}
        ></div>

        <div
          className="ball"
          style={{
            width: ballSize + 'px',
            height: ballSize + 'px',
            top: ballTop + 'px',
            left: ballLeft + 'px',
            transform: `translate(-50%,0)`,
          }}
        ></div>

        <div
          hidden={viewDegree === targetDegree}
          className="hint"
          style={{
            top: worldHeight / 4 + 'px',
            bottom: worldHeight + 'px',
            left: worldWidth / 2 + 'px',
            transform: `translate(-50%,0)`,
          }}
        >
          {isLeftRight(targetDegree, viewDegree) ? <>&larr;</> : <>&rarr;</>}
        </div>
      </div>
    </div>
  )
}

export default App
